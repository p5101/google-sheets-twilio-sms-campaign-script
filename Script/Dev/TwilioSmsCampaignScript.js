// -------------------------------------------------------------------------------------------------
// CONFIGURATION ITEMS
// Basic configuration parameters
const sheetName = 'Twilio';

// Twilio
const twilioAPIBaseUrl_1 = 'https://accounts.twilio.com/v1';
const twilioAPIBaseUrl_2 = 'https://api.twilio.com';
const twilioPageSize = 50;

// Worker
const worker_executionTimeout = 50000;

const version = "1.0.0";

// Configuration UI settings - row and column offsets and sizes
const ui_baseRowOffset = 3;
const ui_dataColumnWidth = 2;
const ui_infoBarWidth = 5;
const ui_smsConfigurationFieldOffset = 5;
const ui_smsConfigurationFieldSize = 8;
const ui_smsCampaignControlFieldOffset = 14;
const ui_smsCampaignControlFieldSize = 5;
const ui_smsCampaignControlFieldStartStopOffset = 2;
const ui_twilioAccountConfigurationOffset = 0;
const ui_twilioAccountConfigurationSize = 4;
const ui_gamanFieldOffset = 20;
const ui_gamanFieldSize = 7;

// -------------------------------------------------------------------------------------------------
// VARIABLES
var sheetHandler = SpreadsheetApp.getActiveSpreadsheet();

// FUNCTIONS - INITIALIZATION

/**
 * Init fuction used during script installation process to assign the rights for the script
 */
function init() {}

// -------------------------------------------------------------------------------------------------
// FUNCTIONS - HANDLERS

// 
/**
 * Trigger function used to initialize sheet (first opening of the sheet)
 */
function onOpen() {
  console.info(sheetName + ' > onOpen > start');

  console.info(sheetName + ' > onOpen > checking installed triggers');  

  // Get sheet triggers
  var sheetTriggers = getSheetTriggers();

  // Verify configuration, if needed add triggers
  if (sheetTriggers.open == null) {
    console.info(sheetName + '> onOpen > adding trigger for sheet opening event');
    ScriptApp.newTrigger('triggerOnOpen')
      .forSpreadsheet(sheetHandler)
      .onOpen()
      .create();
    triggerOnOpen();
  }
  if (sheetTriggers.edit == null) {
    console.info(sheetName + '> onOpen > adding trigger for sheet edit event');
    ScriptApp.newTrigger('triggerOnEdit')
      .forSpreadsheet(sheetHandler)
      .onEdit()
      .create();
  }
  // Triggery wykorzystywane do realizacji kampanii
  var twilioSheetHandler = sheetHandler.getSheetByName(sheetName);
  if (twilioSheetHandler.getRange(18, 2).getValue() == 'Start') {
    if (sheetTriggers.runSMSCampaign == null) {
      console.info(sheetName + ' > onOpen > adding campaign run trigger [runSMSCampaignThread].');
      ScriptApp.newTrigger('runSMSCampaignThread')
        .timeBased()
        .everyMinutes(1)
        .create();
    }
    if (sheetTriggers.runSMSCampaignStatus == null) {
      console.info(sheetName + ' > onOpen > adding campaign run trigger [runSMSCampaignStatusThread].');
      ScriptApp.newTrigger('runSMSCampaignStatusThread')
      .forSpreadsheet(sheetHandler)
      .onEdit()
      .create();
    }
  }
  else {
  // Remove triggers if they exist 
    if (sheetTriggers.runSMSCampaign != null) {
       console.info(sheetName + ' > onOpen > removing campaign run trigger [runSMSCampaignThread]'); 
      ScriptApp.deleteTrigger(sheetTriggers.runSMSCampaign);
    }
    if (sheetTriggers.runSMSCampaignStatus != null) {
      console.info(sheetName + ' > onOpen > removing campaign run trigger [runSMSCampaignStatusThread]'); 
      ScriptApp.deleteTrigger(sheetTriggers.runSMSCampaignStatus);
    }
  }

  console.info(sheetName + '> onOpen > end');
}

/**
 * Trigger function executed when sheet opens
 */
function triggerOnOpen() {
  console.info(sheetName + ' > triggerOnOpen > start');
  try {
    console.info(sheetName + ' > triggerOnOpen > checking if Twilio configuration sheet exists');
    var twilioSheetHandler = sheetHandler.getSheetByName(sheetName);
    if ((twilioSheetHandler !== undefined) && (twilioSheetHandler != null)) { 
      console.info(sheetName + ' > triggerOnOpen > ' + '"' + sheetName + '" sheet exists - validating data');
    }  
    else {
      console.info(sheetName + '>' + '"' + sheetName + '" sheet does not exist - creating new configuration sheet');
      renderTwilioAccountSettings();
      renderGamanInfo();
    }
  }
  catch(exception) {
    console.error(sheetName + '> triggerOnOpen > error');
    console.error(exception);
  }
  console.info(sheetName + '> triggerOnOpen > end');
}

// 
/**
 * Trigger function executed when sheet is updated
 * 
 * @param {object} event - event object taht describes what happened in the sheet 
 */
function triggerOnEdit(event) {
  console.info(sheetName + '> triggerOnEdit > start');
  console.info(ScriptApp.getService().getUrl());
  try {
    var dataChangeRange = event.range;

  // Set variable to check if account sid or auth token has changed
    var validateTwilioAuthorization = false;

  // Check if user changed configuration parameter on configuration sheet tab
    if (dataChangeRange.getSheet().getName() == sheetName) {
      renderInfoMessageBox('', 'black', 'lime');
      console.info(sheetName + '> triggerOnEdit > data change event, row [' + dataChangeRange.getRow() + '], column [' + dataChangeRange.getColumn() + ']');

      var twilioSheetHandler = sheetHandler.getSheetByName(sheetName);
  
  // ******************************************************
  // TWILIO account configuration

    // Twilio account sid selected
      if ((dataChangeRange.getRow() == ui_baseRowOffset + 1) && (dataChangeRange.getColumn() == 2)) {
        twilioSheetHandler.getRange(ui_baseRowOffset + 1, 2).setHorizontalAlignment('left');
        twilioSheetHandler.autoResizeColumns(2, 1);
        validateTwilioAuthorization = true;
      }

    // Twilio auth token selected
      if ((dataChangeRange.getRow() == ui_baseRowOffset + 2) && (dataChangeRange.getColumn() == 2)) {
        twilioSheetHandler.getRange(ui_baseRowOffset + 2, 2).setHorizontalAlignment('left');
        twilioSheetHandler.autoResizeColumns(2, 1);
        validateTwilioAuthorization = true;
      }

    // Twilio outbound phone mumber selected
      if ((dataChangeRange.getRow() == ui_baseRowOffset + ui_smsConfigurationFieldOffset + 3) && (dataChangeRange.getColumn() == 2)) {
        twilioSheetHandler
          .getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 3, 2)
            .setHorizontalAlignment('left');
      }

  // ******************************************************
  // SMS campaign configuration changed events

    // Outbound number selected
      if (
            (dataChangeRange.getRow() == ui_baseRowOffset + ui_smsConfigurationFieldOffset + 3) 
            && (dataChangeRange.getColumn() == 2)
      ) {
        // Update SMS Data field (bug with border)
        var smsText = twilioSheetHandler
                  .getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 3, ui_infoBarWidth - ui_dataColumnWidth + 1)
                    .getValue();
        twilioSheetHandler
          .getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 3, ui_infoBarWidth - ui_dataColumnWidth + 1) 
            .setValue('')
            .setValue(smsText); 
      }

    // Data sheet for SMS campaign selected 
      if (
          (dataChangeRange.getRow() == ui_baseRowOffset + ui_smsConfigurationFieldOffset + 4) 
          && (dataChangeRange.getColumn() == 2)
      ) {
      // Get selected sheet name
        var smsDataSheetName = twilioSheetHandler
          .getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 4, 2)
            .getValue();
        
      // Check if selected sheet is on the list
        var sheetExists = false;
        if (
            (smsDataSheetName != sheetName)
            && (smsDataSheetName != sheetName + ' Results')
        ) {
          var sheets = sheetHandler.getSheets();
          for (var sheetsIterator = 0; sheetsIterator < sheets.length; sheetsIterator++) {
            if (sheets[sheetsIterator].getName() == smsDataSheetName) {
              sheetExists = true;
              break;
            }
          }
        }
        
        if (sheetExists) {
      // Display UI message
          renderInfoMessageBox('INFO: Retriving sheet [' + smsDataSheetName + '] available column list', 'white', 'blue');

        // Update SMS Data field (bug with border)
          var smsText = twilioSheetHandler
                    .getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 3, ui_infoBarWidth - ui_dataColumnWidth + 1)
                      .getValue();
          twilioSheetHandler
            .getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 3, ui_infoBarWidth - ui_dataColumnWidth + 1) 
              .setValue('')
              .setValue(smsText); 

        // Clear available column's filed
          twilioSheetHandler
            .getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 5, 2)
              .setValue('')
              .setDataValidation(null);
          console.info(sheetName + '> triggerOnEdit > SMS data sheet selected');
          if ((smsDataSheetName !== undefined) && (smsDataSheetName != null)) {
            var smsDataSheetColumnNames = [];
            var smsDataSheet = SpreadsheetApp.getActive().getSheetByName(smsDataSheetName);
            if ((smsDataSheet !== undefined) && (smsDataSheet != null)) {
              var smsDataSheetLastColumnIndex = smsDataSheet.getLastColumn();
              for(var smsDataSheetLastColumnIterator = 1; smsDataSheetLastColumnIterator <= smsDataSheetLastColumnIndex; smsDataSheetLastColumnIterator++) {
                var smsDataSheetColumnName = '';
                smsDataSheetLastColumnIteratorPart = smsDataSheetLastColumnIterator;
                while (smsDataSheetLastColumnIteratorPart > 0) {
                  smsDataSheetColumnName = String.fromCharCode(65 + (smsDataSheetLastColumnIteratorPart - 1) % 26) + smsDataSheetColumnName;
                  smsDataSheetLastColumnIteratorPart = parseInt((smsDataSheetLastColumnIteratorPart - 1) / 26);
                }
                smsDataSheetColumnNames.push(smsDataSheetColumnName);
              }
              console.info(sheetName + '> triggerOnEdit > SMS data sheet column count [' + smsDataSheetColumnNames.length + ']');
              twilioSheetHandler
                .getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 5, 2)
                  .setDataValidation(SpreadsheetApp.newDataValidation().requireValueInList(smsDataSheetColumnNames)); 

            // Update SMS Data field (bug with border)
              var smsText = twilioSheetHandler
                .getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 3, ui_infoBarWidth - ui_dataColumnWidth + 1)
                  .getValue();
              twilioSheetHandler
                .getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 3, ui_infoBarWidth - ui_dataColumnWidth + 1) 
                  .setValue('')
                  .setValue(smsText);
                  
            // Clear UI message
              renderInfoMessageBox('', 'black', 'lime');
            }
            else {
              if (smsDataSheetName.length > 0)
                renderInfoMessageBox('ERROR: triggerOnEdit event error.\n' + 'Can not find sheet with name [' + smsDataSheetName + '].', 'white', 'red');
              else
                renderInfoMessageBox('ERROR: triggerOnEdit event error.\n' + 'Data sheet name can not be empty.', 'white', 'red');
              twilioSheetHandler
              .getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 5, 2)
                .setValue('')
                .setDataValidation(null);
            }
          }
        }
        else {
          renderInfoMessageBox('ERROR: triggerOnEdit event error.\n' + 'Can not find sheet with name [' + smsDataSheetName + '].', 'white', 'red');
          twilioSheetHandler
            .getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 5, 2)
              .setValue('')
              .setDataValidation(null);
        }
      }
    }

  // Validate SMS campaign settings
    if (
            (dataChangeRange.getRow() >= ui_baseRowOffset + ui_smsConfigurationFieldOffset) 
            && (dataChangeRange.getRow() <= ui_baseRowOffset + ui_smsConfigurationFieldOffset + ui_smsConfigurationFieldSize) 
            && ((dataChangeRange.getColumn() == 2) || (dataChangeRange.getColumn() == ui_infoBarWidth - ui_dataColumnWidth + 1))
    ) {
      var campaignValidationResults = validateTwilioSMSCampaignSettings();
      if (
        (campaignValidationResults)
        && (isNullOrEmpty(twilioSheetHandler.getRange(ui_baseRowOffset + ui_smsCampaignControlFieldOffset, 1).getValue()))
      ) {
        renderTwilioSMSCampaignControls();
      }
      else if (campaignValidationResults) {}
      else {
      // Clear campaign control UI
        twilioSheetHandler
          .deleteRows(ui_baseRowOffset + ui_smsCampaignControlFieldOffset, ui_smsCampaignControlFieldSize);
        twilioSheetHandler
          .insertRows(ui_baseRowOffset + ui_smsCampaignControlFieldOffset, ui_smsCampaignControlFieldSize);  
      }
    }
  // ******************************************************
  // CAMPAIGN control
    if (
            (dataChangeRange.getRow() == ui_baseRowOffset + ui_smsCampaignControlFieldOffset + ui_smsCampaignControlFieldStartStopOffset) 
            && (dataChangeRange.getColumn() == 2)
    ) {
      console.info(sheetName + ' > triggerOnEdit > campaign control button changed state, value [' + twilioSheetHandler.getRange(ui_baseRowOffset + ui_smsCampaignControlFieldOffset + ui_smsCampaignControlFieldStartStopOffset, 2).getValue() + ']'); 
      switch(twilioSheetHandler.getRange(ui_baseRowOffset + ui_smsCampaignControlFieldOffset + ui_smsCampaignControlFieldStartStopOffset, 2).getValue()) {
        case 'Start':
          console.info(sheetName + ' > triggerOnEdit > campaign control start button selected');
          var smsCampaignConfirmCampaigStartResult = SpreadsheetApp.getUi().alert("Do you realy want to start the campaign, please confirm.", SpreadsheetApp.getUi().ButtonSet.OK_CANCEL);
          if(smsCampaignConfirmCampaigStartResult === SpreadsheetApp.getUi().Button.OK) {
            console.info(sheetName + ' > triggerOnEdit > starting campaign');
            renderInfoMessageBox('INFO: SMS Campaign in progress', 'black', 'orange');

          // Add campaign run triggers
            var sheetTriggers = getSheetTriggers();
            if (sheetTriggers.runSMSCampaign == null) {
              console.info(sheetName + ' > triggerOnEdit > adding campaign trigger [runSMSCampaignThread]');
              ScriptApp.newTrigger('runSMSCampaignThread')
                .timeBased()
                .everyMinutes(1)
                .create();
            }
            if (sheetTriggers.runSMSCampaignStatus == null) {
              console.info(sheetName + ' > triggerOnEdit > adding campaign trigger [runSMSCampaignStatusThread]');
              ScriptApp.newTrigger('runSMSCampaignStatusThread')
                .timeBased()
                .everyMinutes(1)
                .create();
            }
          // Set start time
            twilioSheetHandler
              .getRange(ui_baseRowOffset + ui_smsCampaignControlFieldOffset + 2, ui_infoBarWidth - ui_dataColumnWidth + 2)
                .setValue(Utilities.formatDate(new Date(), Session.getScriptTimeZone(), 'yyyy-MM-dd HH:mm:ss'));
            twilioSheetHandler
              .getRange(ui_baseRowOffset + ui_smsCampaignControlFieldOffset + 3, ui_infoBarWidth - ui_dataColumnWidth + 2)
                .setValue('---');
            twilioSheetHandler
              .autoResizeColumns(1,ui_infoBarWidth - ui_dataColumnWidth + 2);
          // Start the campaign
            twilioExecuteSMSCampaign();
          }
          break;
        case 'Stop':
          break;
        default:
          break;
      }
      return;
    }


  // ******************************************************
  // General functions

    // Check if validate Twilio account
    if (validateTwilioAuthorization) {

    // Display UI message
      renderInfoMessageBox('INFO: Pulling Twilio accounts details', 'white', 'blue');

    // Clear UI
      twilioSheetHandler
        .deleteRows(ui_baseRowOffset + ui_smsCampaignControlFieldOffset, ui_smsCampaignControlFieldSize);
      twilioSheetHandler
        .insertRows(ui_baseRowOffset + ui_smsCampaignControlFieldOffset, ui_smsCampaignControlFieldSize);
      twilioSheetHandler
        .deleteRows(ui_baseRowOffset + ui_smsConfigurationFieldOffset, ui_smsConfigurationFieldSize + 1);
      twilioSheetHandler
        .insertRows(ui_baseRowOffset + ui_smsConfigurationFieldOffset, ui_smsConfigurationFieldSize + 1);

      if (
        (!isNullOrEmpty(twilioSheetHandler.getRange(ui_baseRowOffset + 1, 2).getDisplayValue())) 
        && (!isNullOrEmpty(twilioSheetHandler.getRange(ui_baseRowOffset + 2, 2).getDisplayValue()))
      ) {
        var twilioAccountValidationResult = twilioCheckAccountCredentials(twilioSheetHandler.getRange(ui_baseRowOffset + 1, 2).getDisplayValue(), twilioSheetHandler.getRange(ui_baseRowOffset + 2, 2).getDisplayValue());
        if (twilioAccountValidationResult.result) {
          var twilioSMSPhoneNumbers = twilioGetPhoneNumbers(twilioSheetHandler.getRange(ui_baseRowOffset + 1, 2).getDisplayValue(), twilioSheetHandler.getRange(ui_baseRowOffset + 2, 2).getDisplayValue());
          if (twilioSMSPhoneNumbers != null) {
          // Clear UI message (all OK)
            renderInfoMessageBox('', 'black', 'lime');
          // Render SMS Campaign UI
            renderTwilioSMSCampaignSettings(twilioSMSPhoneNumbers);
          }
        }
      }
      else {
        renderInfoMessageBox('', 'black', 'lime');
      }
    }
  }
  catch (exception) {
    console.error(sheetName + ' > triggerOnEdit > error');
    console.error(exception);
    renderInfoMessageBox('ERROR: triggerOnEdit event error.\n' + exception.toString(), 'white', 'red');
  }
  console.info(sheetName + ' > triggerOnEdit > end');
}

// -------------------------------------------------------------------------------------------------
// FUNCTIONS - TWILIO

/**
 * Function used to check Twilio account credentials
 * 
 * @param {string} accountSid - Twilio account sid
 * @param {string} authToken  - Twilio authentication token
 * @returns object
 * {
 *    result  - boolean that indicates that the Twilio account is accessible via provided credentials
 *    error   - error message returned by Twilio (if result is true then field is not visible)
 * }
 */
function twilioCheckAccountCredentials(accountSid, authToken) {
  console.info(sheetName + ' > twilioCheckAccountCredentials > start');
  try {
    var twilioAuthParameetrs = {
      headers : {
            Authorization: "Basic "+ Utilities.base64Encode(accountSid + ':' + authToken, Utilities.Charset.UTF_8)
      }   
    };
    var twilioAuthResponse = UrlFetchApp.fetch(twilioAPIBaseUrl_1 + '/Credentials/PublicKeys', twilioAuthParameetrs);
    if ((twilioAuthResponse.getResponseCode() >= 200) && (twilioAuthResponse.getResponseCode() < 299)) {
      console.info(sheetName + ' > twilioCheckAccountCredentials > user authentication ended successfully');
      console.info(sheetName + ' > twilioCheckAccountCredentials > end');
      return {result: true}
    }
    else {
      renderInfoMessageBox('ERROR: Twilio user authentication failed.', 'white', 'red');
      console.error(sheetName + ' > twilioCheckAccountCredentials > twilio user authentication failed');
      console.error(sheetName + ' > twilioCheckAccountCredentials > ' + twilioAuthResponse.getContentText());
      console.info(sheetName + ' > twilioCheckAccountCredentials > end');
      return {
        result: false,
        error:  twilioAuthResponse.getContentText()
      }
    }
  }
  catch(exception) {
    renderInfoMessageBox('ERROR: Twilio user authentication failed.' + exception.toString(), 'white', 'red');
    console.error(sheetName + ' > twilioCheckAccountCredentials > function error');
    console.error(exception);
    console.info(sheetName + ' > twilioCheckAccountCredentials > end');
    return {
      result: false,
      error: '[' + exception.name + ']' + exception.message
    }
  }
}

/**
 * Function used to pull the SMS capable phone numbers that can be used by the campaign
 * 
 * @param {string} accountSid  - Twilio account sid
 * @param {string} authToken   - Twilio authentication token
 * @returns array[string] - array of phone numbers returned by Twilio that have the ability to send SMS
 */
function twilioGetPhoneNumbers(accountSid, authToken) {
  console.info(sheetName + ' > twilioGetPhoneNumbers > start');
  var twilioNumbers = [];
  try {
    var twilioAuthParameetrs = {
      headers : {
            Authorization: "Basic "+ Utilities.base64Encode(accountSid + ':' + authToken, Utilities.Charset.UTF_8)
      }   
    };
    var twilioAPIUrlSuffix = '/2010-04-01/Accounts/' + accountSid + '/IncomingPhoneNumbers.json?PageSize=' + twilioPageSize;

    while (1 == 1) {
      console.info(sheetName + ' > twilioGetPhoneNumbers > calling API URL [' + twilioAPIUrlSuffix + ']');
      var twilioNumbersResponse = UrlFetchApp.fetch(twilioAPIBaseUrl_2 + twilioAPIUrlSuffix, twilioAuthParameetrs);
      if ((twilioNumbersResponse.getResponseCode() >= 200) && (twilioNumbersResponse.getResponseCode() < 299)) {

      // Checking if response has [incoming_phone_numbers] object
        var twilioNumbersResponseJSON = JSON.parse(twilioNumbersResponse.getContentText());
        if (twilioNumbersResponseJSON.hasOwnProperty('incoming_phone_numbers')) {
          var twilioIncommingPhoneNumbers = twilioNumbersResponseJSON.incoming_phone_numbers;
          for(var twilioIncommingPhoneNumbersIterator = 0; twilioIncommingPhoneNumbersIterator < twilioIncommingPhoneNumbers.length; twilioIncommingPhoneNumbersIterator++) {
          // Search only for phone numbers that have SMS capabality
            if (
              (twilioIncommingPhoneNumbers[twilioIncommingPhoneNumbersIterator].hasOwnProperty('phone_number')) 
              && (twilioIncommingPhoneNumbers[twilioIncommingPhoneNumbersIterator].hasOwnProperty('capabilities')) 
              && (twilioIncommingPhoneNumbers[twilioIncommingPhoneNumbersIterator].capabilities.hasOwnProperty('sms')) 
              && (twilioIncommingPhoneNumbers[twilioIncommingPhoneNumbersIterator].capabilities.sms) 
            )  {
              console.info(sheetName + ' > twilioGetPhoneNumbers > adding new sms numer [' + twilioIncommingPhoneNumbers[twilioIncommingPhoneNumbersIterator].phone_number + ']')
              twilioNumbers.push(twilioIncommingPhoneNumbers[twilioIncommingPhoneNumbersIterator].phone_number.replace('+',''));
            }
          }
        }
      // Check if Twilio has more pages with numbers - if has continue pulling
        if ((twilioNumbersResponseJSON.hasOwnProperty('next_page_uri')) && (twilioNumbersResponseJSON.next_page_uri != null)){
          twilioAPIUrlSuffix = twilioNumbersResponseJSON.next_page_uri;
        }
        else
          break;
      }
      else {
        console.error(sheetName + ' > twilioGetPhoneNumbers > twilio get phone numbers failed');
        console.error(sheetName + ' > twilioGetPhoneNumbers > ' + twilioNumbersResponse.getContentText());
        twilioNumbers = null;
        break;
      }
    }
  }
  catch(exception) {
    console.error(sheetName + ' > twilioGetPhoneNumbers > function error');
    console.error(exception);
    renderInfoMessageBox('ERROR: Twilio get SMS phone numbers error.\n' + exception.toString(), 'white', 'red');
    twilioNumbers = null;
  }
  console.info(sheetName + ' > twilioGetPhoneNumbers > end');
  return twilioNumbers;
}

// Function used to send SMS to provided phone number
/**
 * 
 * @param {string} accountSid    - Twilio account sid
 * @param {string} authToken     - Twilio authentication token
 * @param {string} smsFromNumber - SMS campaign number (number that will be seen by customer)/SMS From number
 * @param {string} smsToNumber   - SMS customer number/SMS To number
 * @param {string} smsMessge     - SMS message content that will be sent
 * @returns object
 * {
 *    sid           - sms message returned from Twilio API, not empty if sms has been accepted by Twilio
 *    status        - sms delivery status, one of the values [Accepted, Scheduled, Queued, Sending, Sent, Receiving, Received, Delivered, Undelivered, Failed, Read, Error]
 *    dateSent      - date of the message delivery
 *    errorMessage  - error message returned by Twilio api (if successfull then field not visible)
 * }
 */
function twilioSendSMS(accountSid, authToken, smsFromNumber, smsToNumber, smsMessge) {
  console.info(sheetName + ' > twilioSendSMS > start');

  try {
  // Validate SMS to number
    var smsToNumberAfterValidation = smsToNumber;
    smsToNumberAfterValidation = smsToNumberAfterValidation
      .replace(/ /g, '')
      .replace(/\(/g, '')
      .replace(/\)/g, '')
      .replace(/-/g, '');
    smsToNumberAfterValidation = (smsToNumberAfterValidation.indexOf('+') == 0 ? '' : '+') + smsToNumberAfterValidation;

    console.info(sheetName + ' > twilioSendSMS > sending sms to nunmber [' + smsToNumberAfterValidation + ']');

  // Prepare request content 
    var requestContent =  {
      'Body' : smsMessge,
      'From' : '+' + smsFromNumber,
      'To' : smsToNumberAfterValidation
    }

    var twilioSendSMSParameetrs = {
      followRedirects : true,
      headers : {
            Authorization: "Basic "+ Utilities.base64Encode(accountSid + ':' + authToken, Utilities.Charset.UTF_8)
      },
      method : 'post',
      muteHttpExceptions : true,
      payload : requestContent  
    };
    var twilioSendSMSResponse = UrlFetchApp.fetch(twilioAPIBaseUrl_2 + '/2010-04-01/Accounts/' + accountSid + '/Messages.json', twilioSendSMSParameetrs);

    if ((twilioSendSMSResponse.getResponseCode() >= 200) && (twilioSendSMSResponse.getResponseCode() < 299)) {
      var twilioSendSMSResponseJSON = JSON.parse(twilioSendSMSResponse.getContentText());
      console.info(sheetName + '> twilioSendSMS > end');
      return {
        sid : twilioSendSMSResponseJSON.sid,
        status : setFirstLetterToUpperCase(twilioSendSMSResponseJSON.status),
        dateSent : twilioSendSMSResponseJSON.date_sent
      }
    }
    else {
      console.error(sheetName + ' > twilioSendSMS > twilio send sms to number [' + smsToNumber + '] failed');
      console.error(sheetName + ' > twilioSendSMS > ' + twilioSendSMSResponse.getContentText());
      console.info(sheetName + '> twilioSendSMS > end');
      return {
        sid : '',
        status : 'Error',
        errorMessage : twilioSendSMSResponse.getContentText()
      }
    } 
  }
  catch (exception) {
    console.error(sheetName + ' > twilioSendSMS > twilio send sms to number [' + smsToNumber + '] failed');
    console.error(exception);   
    return {
        sid : '',
        status : 'Error',
        errorMessage : exception.toString()
      }
  }
}

/**
 * Funcion used to retrieve SMS delivery status based on the sms message sid
 * 
 * @param {string} accountSid - Twilio account sid
 * @param {string} authToken  - Twilio authentication token
 * @param {string} smsSid     - message id for which the status will be pulled
 * @returns object
 * {
 *    status        - sms delivery status, one of the values [Accepted, Scheduled, Queued, Sending, Sent, Receiving, Received, Delivered, Undelivered, Failed, Read, Error]
 *    dateSent      - date of the message delivery
 *    errorMessage  - error message returned by Twilio api (if successfull then empty)
 * }
 */
function twilioSendSMSStatus(accountSid, authToken, smsSid) {
  console.info(sheetName + ' > twilioSendSMSStatus > start');  
  try {
    var twilioSendSMSParameetrs = {
      followRedirects : true,
      headers : {
            Authorization: "Basic "+ Utilities.base64Encode(accountSid + ':' + authToken, Utilities.Charset.UTF_8)
      },
      method : 'get',
      muteHttpExceptions : true 
    };
    var twilioSendSMSResponse = UrlFetchApp.fetch(twilioAPIBaseUrl_2 + '/2010-04-01/Accounts/' + accountSid + '/Messages/' + smsSid + '.json', twilioSendSMSParameetrs);

    if ((twilioSendSMSResponse.getResponseCode() >= 200) && (twilioSendSMSResponse.getResponseCode() < 299)) {
      var twilioSendSMSResponseJSON = JSON.parse(twilioSendSMSResponse.getContentText());
      console.info(sheetName + '> twilioSendSMSStatus > end');
      return {
        status : setFirstLetterToUpperCase(twilioSendSMSResponseJSON.status),
        dateSent : twilioSendSMSResponseJSON.date_sent,
        errorMessage : twilioSendSMSResponseJSON.error_message
      }
    }
    else {
      console.error(sheetName + ' > twilioSendSMSStatus > twilio send sms status, sms sid [' + smsSid + '] failed');
      console.error(sheetName + ' > twilioSendSMSStatus > ' + twilioSendSMSResponse.getContentText());
      console.info(sheetName + '> twilioSendSMSStatus > end');
      return {
        status : 'Error',
        errorMessage : twilioSendSMSResponse.getContentText()
      }
    }
  }
  catch (exception) {
    console.error(sheetName + ' > twilioSendSMSStatus > twilio send sms status, sms sid [' + smsSid + '] failed');
    console.error(exception);   
    return {
        status : 'Error',
        errorMessage : exception.toString()
      }
  }
}

/**
 * Function used to start Twilio based sms campaign 
 */
function twilioExecuteSMSCampaign() {
  console.info(sheetName + ' > twilioExecuteSMSCampaign > start');
  try {

  // Create result sheet, remove results sheet if already exists
    console.info(sheetName + ' > twilioExecuteSMSCampaign > check if results sheet exists, if not create it');
    var resultDataSheetHandler = SpreadsheetApp.getActive().getSheetByName(sheetName + ' Results');
    if ((resultDataSheetHandler !== undefined) && (resultDataSheetHandler != null)) {
      SpreadsheetApp.getActive().deleteSheet(resultDataSheetHandler);
    }
    var dataSheetColumnNames = [
      'Record status',
      'Sheet name',
      'Row index',
      'Phone number',
      'Date created',
      'Date sent',
      'SMS Sid',
      'SMS delivery status',
      'SMS delivery detals'
    ];
    resultDataSheetHandler = SpreadsheetApp.getActive().insertSheet();
    resultDataSheetHandler
      .deleteRows(2, 998);
    resultDataSheetHandler
      .setName(sheetName + ' Results')
      .setTabColor('red')
       .getRange(1, 1, 1, dataSheetColumnNames.length)
        .setBackground('darkblue')
        .setFontColor('white')
        .setFontWeight('bold')
        .setBorder(true, true, true, true, true, true, 'white', SpreadsheetApp.BorderStyle.SOLID)
        .setValues([dataSheetColumnNames]);
    resultDataSheetHandler
      .autoResizeColumns(1,dataSheetColumnNames.length)
      .setFrozenRows(1);
  }
  catch (exception) {
    console.error(sheetName + ' > twilioExecuteSMSCampaign > failure');
    console.error(exception);
    for (var prop in exception) {  
       console.error('property: '+ prop + ' value: ['+ exception[prop]+ ']');
    } 
  }
  console.info(sheetName + ' > twilioExecuteSMSCampaign > end');
}

// -------------------------------------------------------------------------------------------------
// FUNCTIONS - RENDER UI

/**
 * Function used to render an information message box on the Twilio sheet
 * @param {string} message          - message that will be presented in UI 
 * @param {string} fontColor        - name of the font color of the message (name or hash)
 * @param {string} backgroundColor  - name of the font color of the message (name or hash)
 */
function renderInfoMessageBox(message, fontColor, backgroundColor) {
  try {
    var twilioSheetHandler = sheetHandler.getSheetByName(sheetName);
    twilioSheetHandler
      .getRange(1,1)
        .setFontColor(fontColor)
        .setBackground(backgroundColor);
    twilioSheetHandler
      .getRange(1,2)
        .setValue(message)
        .setFontColor(fontColor)     
        .setBackground(backgroundColor)
        .setWrap(true);
  }
  catch(exception) {
    console.error(sheetName + '>' + 'renderInfoMessageBox error');
    console.error(exception);
  }
}

/**
 * Render UI for Twilio account details
 */
function renderTwilioAccountSettings() {
  try {
  // Create new sheet for Twilio
    var twilioSheetHandler = sheetHandler.insertSheet();
    twilioSheetHandler.setName(sheetName);
    sheetHandler.moveActiveSheet(1);
    twilioSheetHandler.setTabColor('red');
    twilioSheetHandler.activate();

  // Render info bar
    twilioSheetHandler
      .getRange(1, 1)
        .setValue('Message Box')
        .setHorizontalAlignment('left')
        .setVerticalAlignment('top')
        .setFontWeight('bold')
        .setBackground('lime');
    twilioSheetHandler
      .getRange(1, 2)
        .setFontWeight('bold')
        .setBackground('lime');
    twilioSheetHandler
      .getRange(1, 2, 1, ui_infoBarWidth - 1)
        .merge();

  // Add Twilio API fields data fields
    console.info(sheetName + '>' + 'twilio sheet - adding configuration data fields');
    twilioSheetHandler
      .getRange(ui_baseRowOffset, 1)
        .setValue('Twilio Account configuration')
        .setBackground('red')
        .setFontColor('white')
        .setFontSize(12)
        .setFontWeight('bold');
    twilioSheetHandler
      .getRange(ui_baseRowOffset + ui_twilioAccountConfigurationOffset, 1, 1, ui_infoBarWidth)
        .merge();

  // Background
    twilioSheetHandler
      .getRange(ui_baseRowOffset + ui_twilioAccountConfigurationOffset + 1, 1, ui_twilioAccountConfigurationSize - 1, ui_infoBarWidth)
        .setBackground('#FFDFDD');

  // Twilio Account SID 
    twilioSheetHandler
      .getRange(ui_baseRowOffset + ui_twilioAccountConfigurationOffset + 1, 1)
        .setValue('Account SID');
    twilioSheetHandler
      .getRange(ui_baseRowOffset + ui_twilioAccountConfigurationOffset + 1, 2)
        .setBackground('white')
        .setHorizontalAlignment('left');
    twilioSheetHandler
      .getRange(ui_baseRowOffset + ui_twilioAccountConfigurationOffset + 1, 2, 1, ui_dataColumnWidth - 1)
        .merge();

  // Twilio Auth Token
    twilioSheetHandler
      .getRange(ui_baseRowOffset + ui_twilioAccountConfigurationOffset + 2, 1)
        .setValue('Auth Token');
    twilioSheetHandler
      .getRange(ui_baseRowOffset + ui_twilioAccountConfigurationOffset + 2, 2)
        .setBackground('white')
        .setHorizontalAlignment('left');
    twilioSheetHandler
      .getRange(ui_baseRowOffset + ui_twilioAccountConfigurationOffset + 2, 2, 1, ui_dataColumnWidth - 1)
        .merge();

  // Add border to field
    twilioSheetHandler
      .getRange(ui_baseRowOffset + ui_twilioAccountConfigurationOffset + 1, 1, 2, ui_dataColumnWidth)
        .setBorder(true, true, true, true, true, true);

  // Auto resize columns
      twilioSheetHandler.autoResizeColumns(1,ui_dataColumnWidth);
      console.info(sheetName + '>' + 'twilio sheet created');
  }
  catch (exception) {
    console.error(sheetName + '>' + 'twilio renderTwilioAccountSettings() error');
    console.error(exception);
  }
}

/**
 * Render UI for SMS Campaign Settings/Configuration
 * @param {array[string]} twilioSMSPhoneNumbers - array of Twilio phone numbers that have SMS capabality 
 */
function renderTwilioSMSCampaignSettings(twilioSMSPhoneNumbers) {
  console.info(sheetName + '>' + 'renderTwilioSMSCampaignSettings start');
  try {
    console.info('Twilio sheet - adding SMS default data field');

    var twilioSheetHandler = sheetHandler.getSheetByName(sheetName);

  // Info bar
    twilioSheetHandler
      .getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset, 1)
        .setValue('SMS Campaign Configuration')
        .setBackground('red')
        .setFontColor('white')
        .setFontSize(12)
        .setFontWeight('bold');
    twilioSheetHandler
      .getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset, 1, 1, ui_infoBarWidth)
        .merge();

  // Background
    twilioSheetHandler
      .getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 1, 1, ui_smsConfigurationFieldSize - 1, ui_infoBarWidth)
        .setBackground('#FFDFDD');

  // General configuration message (left menu)
    twilioSheetHandler
      .getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 2, 1)
        .setValue('General SMS Configuration')
        .setBackground('red')
        .setFontColor('white')
        .setFontSize(12)
        .setFontWeight('bold');
    twilioSheetHandler
      .getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 2, 1, 1, ui_dataColumnWidth)
        .merge();

  // Twilio outbound number (left menu)
    twilioSheetHandler
      .getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 3, 1)
        .setValue('Outbound number')
        .setHorizontalAlignment('left');
    twilioSheetHandler
      .getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 3, 2)
        .setBackground('white')
        .setDataValidation(SpreadsheetApp.newDataValidation().requireValueInList(twilioSMSPhoneNumbers));
    twilioSheetHandler
      .getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 3, 2, 1, ui_dataColumnWidth - 1)
        .mergeAcross();

  // Contact data source (left menu)
    var sheets = sheetHandler.getSheets();
    var sheetsNames = [];
    for (var sheetsIterator = 0; sheetsIterator < sheets.length; sheetsIterator++)
      if ((sheets[sheetsIterator].getName() != sheetName) && (sheets[sheetsIterator].getName() != sheetName + ' Results'))
        sheetsNames.push(sheets[sheetsIterator].getName());
    twilioSheetHandler
      .getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 4, 1)
        .setValue('Data sheet')
        .setHorizontalAlignment('left');
    twilioSheetHandler
      .getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 4, 2)
        .setBackground('white')
        .setDataValidation(SpreadsheetApp.newDataValidation().requireValueInList(sheetsNames));
    twilioSheetHandler
      .getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 4, 2, 1, ui_dataColumnWidth - 1)
        .mergeAcross();

  // Contact phone number data source column (left menu)
    twilioSheetHandler
      .getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 5, 1)
        .setValue('Phone number column')
        .setHorizontalAlignment('left');
    twilioSheetHandler
      .getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 5, 2, 1, ui_dataColumnWidth - 1)
        .merge();

    twilioSheetHandler
      .getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 5, 2)
        .setBackground('white');

  // Result type selection (left menu)
    twilioSheetHandler
      .getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 6, 1)
        .setValue('Store result in')
        .setHorizontalAlignment('left');
    var smsResultsTarget = [
      'New sheet'
    ];
    twilioSheetHandler
      .getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 6, 2)
        .setBackground('white')
        .setDataValidation(SpreadsheetApp.newDataValidation().requireValueInList(smsResultsTarget))
        .setValue(smsResultsTarget[0]);
    twilioSheetHandler
      .getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 6, 2, 1, ui_dataColumnWidth - 1)
        .merge();  

  var smsConfigurationFieldLeftMenuSize = 5;
  twilioSheetHandler
    .getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 2, 1, smsConfigurationFieldLeftMenuSize, 2)
      .setBorder(true, true, true, true, true, true);

  // SMS editor field bar (right menu)
    twilioSheetHandler
      .getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 2, ui_infoBarWidth - ui_dataColumnWidth + 1)
        .setValue('SMS message')
        .setBackground('red')
        .setFontColor('white')
        .setFontSize(12)
        .setFontWeight('bold')
        .setBorder(true, true, true, true, true, true);
    twilioSheetHandler
      .setColumnWidth(ui_infoBarWidth - ui_dataColumnWidth + 1, twilioSheetHandler.getColumnWidth(2));
    twilioSheetHandler
      .getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 2, ui_infoBarWidth - ui_dataColumnWidth + 1, 1, ui_dataColumnWidth)
        .merge();

  // SMS editor data field
    twilioSheetHandler
      .getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 3, ui_infoBarWidth - ui_dataColumnWidth + 1)
        .setHorizontalAlignment('left')
        .setVerticalAlignment('top')
        .setWrap(true)
        .setBackground('white')
        .setBorder(true, true, true, true, false, false);
    twilioSheetHandler
      .getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 3, ui_infoBarWidth - ui_dataColumnWidth + 1, 4, ui_dataColumnWidth)
        .merge();

  }
  catch (exception) {
    console.error(sheetName + '>' + 'twilio renderTwilioSMSCampaignSettings() error');
    console.error(exception);
  }
  console.info(sheetName + '>' + 'renderTwilioSMSCampaignSettings end');
}

/**
 * Render UI for SMS Campaig Control (ex. Start/Stop) 
 */
function renderTwilioSMSCampaignControls() {
  console.info(sheetName + ' > renderTwilioSMSCampaignControls > start');

  var twilioSheetHandler = sheetHandler.getSheetByName(sheetName);

  // Info bar
  twilioSheetHandler
    .getRange(ui_baseRowOffset + ui_smsCampaignControlFieldOffset, 1)
      .setValue('SMS Campaign Control')
      .setBackground('darkblue')
      .setFontColor('white')
      .setFontSize(12)
      .setFontWeight('bold');
  twilioSheetHandler
    .getRange(ui_baseRowOffset + ui_smsCampaignControlFieldOffset, 1, 1, ui_infoBarWidth)
      .merge();
  // Background
  twilioSheetHandler
    .getRange(ui_baseRowOffset + ui_smsCampaignControlFieldOffset + 1, 1, ui_smsCampaignControlFieldSize - 1, ui_infoBarWidth)
      .setBackground('#82CAFF');


  // Campaign start/stop (left menu)
  twilioSheetHandler
    .getRange(ui_baseRowOffset + ui_smsCampaignControlFieldOffset + ui_smsCampaignControlFieldStartStopOffset, 1)
      .setValue('Campaign status')
      .setHorizontalAlignment('left')
      .setBackground('#2B60DE')
      .setFontColor('white');
  var campaignStatuses = [
    'Start',
    'Stop'
  ];
  twilioSheetHandler
    .getRange(ui_baseRowOffset + ui_smsCampaignControlFieldOffset + ui_smsCampaignControlFieldStartStopOffset, 2)
      .setDataValidation(SpreadsheetApp.newDataValidation().requireValueInList(campaignStatuses))
      .setValue('Stop')
      .setBackground('white');
  twilioSheetHandler
    .getRange(ui_baseRowOffset + ui_smsCampaignControlFieldOffset + ui_smsCampaignControlFieldStartStopOffset, 2, 1, ui_dataColumnWidth - 1)
      .mergeAcross();

  // Campaign start time (right menu)
  twilioSheetHandler
    .getRange(ui_baseRowOffset + ui_smsCampaignControlFieldOffset + 2, ui_infoBarWidth - ui_dataColumnWidth + 1)
      .setValue('Campaign start')
      .setHorizontalAlignment('left')
      .setBackground('#2B60DE')
      .setFontColor('white');
  twilioSheetHandler
    .getRange(ui_baseRowOffset + ui_smsCampaignControlFieldOffset + 2, ui_infoBarWidth - ui_dataColumnWidth + 2)
      .setValue('---')
      .setHorizontalAlignment('center')
      .setBackground('white');

  // Campaign end time (right menu)
  twilioSheetHandler
    .getRange(ui_baseRowOffset + ui_smsCampaignControlFieldOffset + 3, ui_infoBarWidth - ui_dataColumnWidth + 1)
      .setValue('Campaign end')
      .setHorizontalAlignment('left')
      .setBackground('#2B60DE')
      .setFontColor('white');
  twilioSheetHandler
    .getRange(ui_baseRowOffset + ui_smsCampaignControlFieldOffset + 3, ui_infoBarWidth - ui_dataColumnWidth + 2)
      .setValue('---')
      .setHorizontalAlignment('center')
      .setBackground('white');

  // Borders
  var ui_twilioSMSCampaigControlLeftMenuSize = 1;
  var ui_twilioSMSCampaigControlRightMenuSize = 2;
  twilioSheetHandler
    .getRange(ui_baseRowOffset + ui_smsCampaignControlFieldOffset + ui_smsCampaignControlFieldStartStopOffset, 1, ui_twilioSMSCampaigControlLeftMenuSize, 2)
      .setBorder(true, true, true, true, true, true);
  twilioSheetHandler
    .getRange(ui_baseRowOffset + ui_smsCampaignControlFieldOffset + 2, ui_infoBarWidth - ui_dataColumnWidth + 1, ui_twilioSMSCampaigControlRightMenuSize, 2)
      .setBorder(true, true, true, true, true, true);

  console.info(sheetName + ' > renderTwilioSMSCampaignControls > end');
}

/**
 * Render UI for copyrigh - Gaman  
 */
function renderGamanInfo() {
    console.info(sheetName + ' > renderGamanInfo > start');

    var twilioSheetHandler = sheetHandler.getSheetByName(sheetName);

  // Info bar
    twilioSheetHandler
      .getRange(ui_baseRowOffset + ui_gamanFieldOffset, 1)
        .setValue('Created by')
        .setBackground('darkblue')
        .setFontColor('white')
        .setFontSize(12)
        .setFontWeight('bold');
    twilioSheetHandler
      .getRange(ui_baseRowOffset + ui_gamanFieldOffset, 1, 1, ui_infoBarWidth)
        .merge();

  // Background
    twilioSheetHandler
      .getRange(ui_baseRowOffset + ui_gamanFieldOffset + 1, 1, ui_gamanFieldSize, ui_infoBarWidth)
        .setBackground('#82CAFF');

  // Logo
    twilioSheetHandler
      .getRange(ui_baseRowOffset + ui_gamanFieldOffset + 1, 1, 4, ui_infoBarWidth)
        .merge();
    twilioSheetHandler
      .getRange(ui_baseRowOffset + ui_gamanFieldOffset + 1, 1)
        .setValue('=IMAGE("https://gaman-gt.com/img/logo.png")')
        .setHorizontalAlignment('center');

  // Website
    twilioSheetHandler
      .getRange(ui_baseRowOffset + ui_gamanFieldOffset + 5, 1)
        .setValue('Website')
        .setFontWeight('bold')
        .setHorizontalAlignment('left')
        .setBackground('#2B60DE')
        .setFontColor('white');
    twilioSheetHandler
      .getRange(ui_baseRowOffset + ui_gamanFieldOffset + 5, 2, 1, ui_infoBarWidth - 1)
        .merge()
        .setValue('=HYPERLINK("https://gaman-gt.com")')
        .setFontColor('black')
        .setBackground('white');

  // Email
    twilioSheetHandler
      .getRange(ui_baseRowOffset + ui_gamanFieldOffset + 6, 1)
        .setValue('Email')
        .setFontWeight('bold')
        .setHorizontalAlignment('left')
        .setBackground('#2B60DE')
        .setFontColor('white');
    twilioSheetHandler
      .getRange(ui_baseRowOffset + ui_gamanFieldOffset + 6, 2, 1, ui_infoBarWidth - 1)
        .merge()
        .setValue('=HYPERLINK("mailto:info@gaman-gt.com")')
        .setFontColor('black')
        .setBackground('white');

  // Version
    twilioSheetHandler
      .getRange(ui_baseRowOffset + ui_gamanFieldOffset + 7, 1)
        .setValue('Version')
        .setFontWeight('bold')
        .setHorizontalAlignment('left')
        .setBackground('#2B60DE')
        .setFontColor('white');
    twilioSheetHandler
      .getRange(ui_baseRowOffset + ui_gamanFieldOffset + 7, 2, 1, ui_infoBarWidth - 1)
        .merge()
        .setValue(version)
        .setFontColor('black')
        .setBackground('white');

  // Borders
    twilioSheetHandler
      .getRange(ui_baseRowOffset + ui_gamanFieldOffset, 1, ui_gamanFieldSize + 1, ui_infoBarWidth)
        .setBorder(true, true, true, true, true, true);

    console.info(sheetName + ' > renderGamanInfo > start');
}

// -------------------------------------------------------------------------------------------------
// FUNCTIONS - VALIDATORS

/**
 * Function used to validate user input for the sms campaign configuration section
 * 
 * @returns boolean (true, false) - value indicates if the validation process was successfull or some of the input fields don't
 *     meet the validation requirements.
 */
function validateTwilioSMSCampaignSettings() {
  console.info(sheetName + ' > validateTwilioSMSCampaignSettings > start');
  var twilioSheetHandler = sheetHandler.getSheetByName(sheetName);
  var validationResult = true;

  if (
    (!isNullOrEmpty(twilioSheetHandler.getRange(ui_baseRowOffset + 1, 2).getValue())) 
    && (!isNullOrEmpty(twilioSheetHandler.getRange(ui_baseRowOffset + 2, 2).getValue()))
  ) {
    console.info(sheetName + ' > validateTwilioSMSCampaignSettings > validate campaign control fields');
  // Validate outbound phone number
    if (
      (!isNullOrEmpty(twilioSheetHandler.getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 3, 2).getValue()))
      && (twilioSheetHandler.getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 3, 2).getDataValidation().getCriteriaValues()[0].includes(twilioSheetHandler.getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 3, 2).getValue().toString()))
    ) {
      twilioSheetHandler
        .getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 3, 2)
          .setBorder(true, true, true, true, true, true, 'black', SpreadsheetApp.BorderStyle.SOLID);
    }
    else {
      twilioSheetHandler
        .getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 3, 2)
          .setBorder(true, true, true, true, true, true, 'red', SpreadsheetApp.BorderStyle.SOLID_MEDIUM);
      validationResult = false;
    }

  // Validate data datasheet
    if (
      (!isNullOrEmpty(twilioSheetHandler.getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 4, 2).getValue())) 
      && (twilioSheetHandler.getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 4, 2).getDataValidation().getCriteriaValues()[0].includes(twilioSheetHandler.getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 4, 2).getValue().toString()))
    ) {
      twilioSheetHandler
        .getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 4, 2)
          .setBorder(true, true, true, true, true, true, 'black', SpreadsheetApp.BorderStyle.SOLID);
      twilioSheetHandler
        .getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 5, 2)
          .setBackground('white');
      if (
        (!isNullOrEmpty(twilioSheetHandler.getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 5, 2).getValue())) 
        && (twilioSheetHandler.getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 5, 2).getDataValidation().getCriteriaValues()[0].includes(twilioSheetHandler.getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 5, 2).getValue().toString()))
      ) {
        twilioSheetHandler
          .getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 5, 2)
            .setBorder(true, true, true, true, true, true, 'black', SpreadsheetApp.BorderStyle.SOLID);
      }
      else {
        twilioSheetHandler
          .getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 5, 2)
            .setBorder(true, true, true, true, true, true, 'red', SpreadsheetApp.BorderStyle.SOLID_MEDIUM);
        validationResult = false;
      }
    }
    else {
      twilioSheetHandler
        .getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 4, 2)
          .setBorder(true, true, true, true, true, true, 'red', SpreadsheetApp.BorderStyle.SOLID_MEDIUM);
      twilioSheetHandler
        .getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 5, 2)
          .setBackground('#FFDFDD')
          .setBorder(true, true, true, true, true, true, 'black', SpreadsheetApp.BorderStyle.SOLID);
      validationResult = false;
    }

  // Validate results sheet
    if (
      (!isNullOrEmpty(twilioSheetHandler.getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 6, 2).getValue())) 
      && (twilioSheetHandler.getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 6, 2).getDataValidation().getCriteriaValues()[0].includes(twilioSheetHandler.getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 6, 2).getValue().toString()))
    ) {
      twilioSheetHandler
        .getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 6, 2)
          .setBorder(true, true, true, true, true, true, 'black', SpreadsheetApp.BorderStyle.SOLID);
    }
    else {
      twilioSheetHandler
        .getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 6, 2)
          .setBorder(true, true, true, true, true, true, 'red', SpreadsheetApp.BorderStyle.SOLID_MEDIUM);
      validationResult = false;
    }

  // Validate SMS Content
    if (!isNullOrEmpty(twilioSheetHandler.getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 3, ui_infoBarWidth - ui_dataColumnWidth + 1).getValue())) {
      twilioSheetHandler
        .getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 3, ui_infoBarWidth - ui_dataColumnWidth + 1)
          .setBorder(true, true, true, true, true, true, 'black', SpreadsheetApp.BorderStyle.SOLID);
    }
    else {
      twilioSheetHandler
        .getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 3, ui_infoBarWidth - ui_dataColumnWidth + 1)
          .setBorder(true, true, true, true, true, true, 'red', SpreadsheetApp.BorderStyle.SOLID_MEDIUM);
      validationResult = false;
    }
  }
  console.info(sheetName + ' > validateTwilioSMSCampaignSettings > end');
  return validationResult;
}

// -------------------------------------------------------------------------------------------------
// WORKER FUNCTIONS

/**
 * Worker thread responisble for sending the SMS messages to Twilio and putting them in the result sheet
 */
function runSMSCampaignThread() {
  console.info(sheetName + ' > runSMSCampaignThread > start');
  try {

  // Get script start time
    var scriptStartDateTime = new Date();

    var twilioSheetHandler = sheetHandler.getSheetByName(sheetName);

  // Get campaign control status
    var smsCampaignControlStatus = twilioSheetHandler.getRange(ui_baseRowOffset + ui_smsCampaignControlFieldOffset + ui_smsCampaignControlFieldStartStopOffset ,2).getValue();
    if (smsCampaignControlStatus == 'Stop') {
      console.info(sheetName + ' > runSMSCampaignThread > campaign stopped');
      console.info(sheetName + ' > runSMSCampaignThread > end');
      return;
    }

  // Get campaign parameters
    var smsCampaignConfiguration = getSMSCampaignConfigurationParameters();

  // Get data sheet parameters
    console.info(sheetName + ' > runSMSCampaignThread > get data sheet parameters');
    var dataSheetHandler = SpreadsheetApp.getActive().getSheetByName(smsCampaignConfiguration.dataSheetName);
    var dataSheetDataRowsCount = dataSheetHandler.getLastRow();

  // Get result sheet parameters
    var resultSheetHandler = SpreadsheetApp.getActive().getSheetByName(sheetName + ' Results');
    var resultSheetDataRowsCount = resultSheetHandler.getLastRow() - 1;
    var dataSheetCurrentRecord = resultSheetDataRowsCount + 1;
    
    console.info(sheetName + ' > runSMSCampaignThread > sms campaign start [' + scriptStartDateTime + '].');

    while (1 == 1) {

  // Check script finish conditions
    // Finish script due to campaign run trigger timeout
      var scriptCurentDateTime = new Date();
      var scriptExecutionTime = scriptCurentDateTime.getTime() - scriptStartDateTime.getTime();
      if (scriptExecutionTime > worker_executionTimeout) {
        console.info(sheetName + ' > runSMSCampaignThread > script excecution time limit [' + scriptExecutionTime + 'ms]');
        console.info(sheetName + ' > runSMSCampaignThread > sms campaign end [' + scriptCurentDateTime + '].');
        break;
      }

    // Finish campaign if all rows processed
      if (dataSheetCurrentRecord > dataSheetDataRowsCount) {
        renderInfoMessageBox('INFO: Pulling SMS delivery statuses for campaign', 'black', 'yellow');
        twilioSheetHandler.getRange(ui_baseRowOffset + ui_smsCampaignControlFieldOffset + ui_smsCampaignControlFieldStartStopOffset ,2).setValue('Stop');
        console.info(sheetName + ' > runSMSCampaignThread > campaign all rows processed.');
        console.info(sheetName + ' > runSMSCampaignThread > sms campaign end [' + scriptCurentDateTime + '].');
      // Remove time triggers
        var sheetTriggers = getSheetTriggers();
        if (sheetTriggers.runSMSCampaign != null)
          ScriptApp.deleteTrigger(sheetTriggers.runSMSCampaign);
      // Update campaign stop time
        twilioSheetHandler
          .getRange(ui_baseRowOffset + ui_smsCampaignControlFieldOffset + 3, ui_infoBarWidth - ui_dataColumnWidth + 2)
          .setValue(Utilities.formatDate(new Date(), Session.getScriptTimeZone(), 'yyyy-MM-dd HH:mm:ss'));
        twilioSheetHandler
          .autoResizeColumns(1,ui_infoBarWidth - ui_dataColumnWidth + 2);
      // Create data filter
        var resultDataSheetHandler = SpreadsheetApp.getActive().getSheetByName(sheetName + ' Results');
        resultDataSheetHandler.getDataRange().createFilter();
        break;
      }
      
      smsCampaignControlStatus = twilioSheetHandler.getRange(ui_baseRowOffset + ui_smsCampaignControlFieldOffset + ui_smsCampaignControlFieldStartStopOffset ,2).getValue();
      if (smsCampaignControlStatus == 'Stop') {
        console.info(sheetName + ' > runSMSCampaignThread > campaign has been stopped.');
      // Remove time triggers
        var sheetTriggers = getSheetTriggers();
        if (sheetTriggers.runSMSCampaign != null)
          ScriptApp.deleteTrigger(sheetTriggers.runSMSCampaign);
      // Update campaign stop time
        twilioSheetHandler
          .getRange(ui_baseRowOffset + ui_smsCampaignControlFieldOffset + 3, ui_infoBarWidth - ui_dataColumnWidth + 2)
            .setValue(Utilities.formatDate(new Date(), Session.getScriptTimeZone(), 'yyyy-MM-dd HH:mm:ss'));
        twilioSheetHandler
          .autoResizeColumns(1,ui_infoBarWidth - ui_dataColumnWidth + 2);
      // Create data filter
        var resultDataSheetHandler = SpreadsheetApp.getActive().getSheetByName(sheetName + ' Results');
        resultDataSheetHandler.getDataRange().createFilter();
        break;
      }

  // Process data
      renderInfoMessageBox('INFO: SMS Campaign in progress', 'black', 'orange');

    // Get record 
      var smsDataRecordRange = smsCampaignConfiguration.smsToNumberColumn + dataSheetCurrentRecord.toString();
      var smsToPhoneNumber = dataSheetHandler.getRange(smsCampaignConfiguration.smsToNumberColumn + dataSheetCurrentRecord.toString()).getValue();
      console.info(sheetName + ' > runSMSCampaignThread > sending sms to phone [' + smsToPhoneNumber + '].');

      // Add data to result sheet
      var smsStatusValues = [
        (((smsToPhoneNumber !== undefined) && (smsToPhoneNumber != null) && (smsToPhoneNumber.toString().length > 0)) ? 'New' : 'Finished'),
        smsCampaignConfiguration.dataSheetName,
        dataSheetCurrentRecord,
        smsToPhoneNumber,
        Utilities.formatDate(new Date(), Session.getScriptTimeZone(), 'yyyy-MM-dd HH:mm:ss'),
        '',
        '',
        (((smsToPhoneNumber !== undefined) && (smsToPhoneNumber != null) && (smsToPhoneNumber.toString().length > 0)) ? 'Unknown' : 'Invalid number'),
        ''
      ];
      if (resultSheetHandler.getMaxRows() < dataSheetCurrentRecord + 1 )
        resultSheetHandler.insertRowAfter(dataSheetCurrentRecord);
      resultSheetHandler
        .getRange(dataSheetCurrentRecord + 1, 1, 1, smsStatusValues.length)
          .setValues([smsStatusValues])
          .setHorizontalAlignment('left')
          .setBorder(true, true, true, true, true, true);
          

      if (((smsToPhoneNumber !== undefined) && (smsToPhoneNumber != null) && (smsToPhoneNumber.toString().length > 0))) {
      // Send sms
        var smsSendResult = twilioSendSMS(
          smsCampaignConfiguration.accountSid,
          smsCampaignConfiguration.authToken,
          smsCampaignConfiguration.smsFromNumber,
          smsToPhoneNumber.toString(),
          smsCampaignConfiguration.smsMessage
        );

      // Update data sheet (api status)
        switch (smsSendResult.status) {
          case 'Error':
            resultSheetHandler
              .getRange(dataSheetCurrentRecord + 1, 1)
                .setValue('Finished');
            resultSheetHandler
              .getRange(dataSheetCurrentRecord + 1, smsStatusValues.length - 1, 1, 2)
                .setValues([[smsSendResult.status, smsSendResult.errorMessage]]);
            break;
          case 'Queued':
            resultSheetHandler
              .getRange(dataSheetCurrentRecord + 1, 1)
                .setValue('In Progress');
            resultSheetHandler
              .getRange(dataSheetCurrentRecord + 1, smsStatusValues.length - 2, 1, 2)
                .setValues([[smsSendResult.sid, smsSendResult.status]]);
            break;
          default:
            console.error(sheetName + ' > runSMSCampaignThread > unknown sms status [' + smsSendResult.status + ']');
            resultSheetHandler
              .getRange(dataSheetCurrentRecord + 1, smsStatusValues.length - 1, 1, 2)
                .setValues([[smsSendResult.status, 'Unknown sms status [' + smsSendResult.status + ']']]);
            break;
        }
        resultSheetHandler
          .autoResizeColumns(1, resultSheetHandler.getLastColumn());
      }

      dataSheetCurrentRecord = dataSheetCurrentRecord + 1;
    }
  }
  catch (exception) {
    console.info(sheetName + ' > runSMSCampaignThread > sms campaign thread error');
    console.error(exception);
  }
  
  console.info(sheetName + ' > runSMSCampaignThread > end');
}

/**
 * Worker thread responisble for updating sms delivery statuses for contacts that have been already queued for delivery
 */
function runSMSCampaignStatusThread() {
  console.info(sheetName + ' > runSMSCampaignStatusThread > start');
  
  try {
    var scriptStartDateTime = new Date();
    var twilioSheetHandler = sheetHandler.getSheetByName(sheetName);
 
  // Check if conditions are met to finish the app.
    // Finish if campaign in stopped state and the time difference between between curent time and stop time exceeds configured limit
    smsCampaignControlStatus = twilioSheetHandler.getRange(ui_baseRowOffset + ui_smsCampaignControlFieldOffset + ui_smsCampaignControlFieldStartStopOffset ,2).getValue();
    if (smsCampaignControlStatus == 'Stop') {
      // Get campaign end date time
        smsCampaignEnDateTimeString = twilioSheetHandler
          .getRange(ui_baseRowOffset + ui_smsCampaignControlFieldOffset + 3, ui_infoBarWidth - ui_dataColumnWidth + 2)
            .getValue()
              .toString();
        var smsCampaignEnDateTime = null;
        try {
          smsCampaignEnDateTime = new Date(smsCampaignEnDateTimeString);
        }
        catch (smsCampaignEnDateTimeParseException) {}
        console.log(smsCampaignEnDateTime);
        if (
          (isNullOrEmpty(smsCampaignEnDateTime))
          || (scriptStartDateTime.getTime() - smsCampaignEnDateTime.getTime() >= 300000)
        ) {
        // Remove time triggers
          var sheetTriggers = getSheetTriggers();
          if (sheetTriggers.runSMSCampaignStatus != null)
            ScriptApp.deleteTrigger(sheetTriggers.runSMSCampaignStatus);
          console.info(sheetName + ' > runSMSCampaignStatusThread > sms campaign status thread end [' + scriptStartDateTime + '].');
          console.info(sheetName + ' > runSMSCampaignThread > end');
          return;
        }
      }
  // Get parameters needed to retrieve status
    // Result sheet parameters
    var resultSheetHandler = SpreadsheetApp.getActive().getSheetByName(sheetName + ' Results');
    // Campaign parameters
    var smsCampaignConfiguration = getSMSCampaignConfigurationParameters();
    // Records data sheet
    var dataSheetHandler = SpreadsheetApp.getActive().getSheetByName(smsCampaignConfiguration.dataSheetName);
    
    console.info(sheetName + ' > runSMSCampaignStatusThread > sms campaign status thread start [' + scriptStartDateTime + '].');

    var smsCampaignProgressRowIndex = 2;

    while (1 == 1) {

  // Check script finish conditions
    // Finish script due to campaign run trigger timeout
      var scriptCurentDateTime = new Date();
      var scriptExecutionTime = scriptCurentDateTime.getTime() - scriptStartDateTime.getTime();
      if (scriptExecutionTime > worker_executionTimeout) {
        console.info(sheetName + ' > runSMSCampaignStatusThread > script excecution time limit [' + scriptExecutionTime + 'ms]');
        console.info(sheetName + ' > runSMSCampaignStatusThread > sms campaign status thread end [' + scriptCurentDateTime + '].');
        break;
      }
  // Process data
    // Get the list of records in not finisched state
      if (resultSheetHandler.getLastRow() > 1) {
        var smsCampaignStatuses = resultSheetHandler.getRange(2, 1, resultSheetHandler.getLastRow() - 1, 1).getValues();
        var smsCampaignProgressHasResult = false;
        for(var smsCampaignProgressIterator = 0; smsCampaignProgressIterator < smsCampaignStatuses.length; smsCampaignProgressIterator++) {
          if (
            (smsCampaignStatuses[smsCampaignProgressIterator][0] !== undefined)
            && (smsCampaignStatuses[smsCampaignProgressIterator][0] != null)
            && (smsCampaignStatuses[smsCampaignProgressIterator][0] != 'Finished')
          ) {
            smsCampaignProgressHasResult = true;
            if (smsCampaignProgressIterator + 2 >= smsCampaignProgressRowIndex) {
              smsCampaignProgressRowIndex = smsCampaignProgressIterator + 2;
              break;
            }
          }
        }

    // Check if all records processed
        if (!smsCampaignProgressHasResult) {
          if (resultSheetHandler.getLastRow() - 1 == dataSheetHandler.getLastRow()) {
            // All records have been processed
            console.info(sheetName + ' > runSMSCampaignStatusThread > all records processed');
            renderInfoMessageBox('INFO: SMS Campaign completed', 'black', 'lime');
            // Remove time triggers
            var sheetTriggers = getSheetTriggers();
            if (sheetTriggers.runSMSCampaignStatus != null)
              ScriptApp.deleteTrigger(sheetTriggers.runSMSCampaignStatus);
            console.info(sheetName + ' > runSMSCampaignStatusThread > sms campaign status thread end [' + scriptStartDateTime + '].');
            console.info(sheetName + ' > runSMSCampaignThread > end');
            break;
          }
          else {
            // Not all recrds have been rocessed - return to begining and check if there is something to process
            smsCampaignProgressRowIndex = 2;
          }
        }
        else {
    // Process current record
          if (smsCampaignStatuses[smsCampaignProgressRowIndex - 2][0] != 'Finished') {
            var smsCampaignMessageSid = resultSheetHandler.getRange(smsCampaignProgressRowIndex, 7).getValue();
            if (
              (smsCampaignMessageSid !== undefined)
              && (smsCampaignMessageSid != null)
              && (smsCampaignMessageSid != '')
            ) {
              // Send status request
              var smsCampaignMessageStatus = twilioSendSMSStatus(
                smsCampaignConfiguration.accountSid,
                smsCampaignConfiguration.authToken,
                smsCampaignMessageSid
              );
              console.info(sheetName + ' > runSMSCampaignThread > sms message sid [' + smsCampaignMessageSid + '], status [' + smsCampaignMessageStatus.status + '].');
              switch(smsCampaignMessageStatus.status) {
                case 'Error':
                case 'Failed':
                case 'Undelivered':
                  resultSheetHandler
                    .getRange(smsCampaignProgressRowIndex, 1)
                      .setValue('Finished');
                  resultSheetHandler
                    .getRange(smsCampaignProgressRowIndex, 6, 1, 2)
                      .setValues([[smsCampaignMessageStatus.status, smsCampaignMessageStatus.errorMessage]]);
                  break;
                case 'Delivered':
                  resultSheetHandler
                    .getRange(smsCampaignProgressRowIndex, 1)
                      .setValue('Finished');
                  resultSheetHandler
                    .getRange(smsCampaignProgressRowIndex, 6)
                      .setValue(Utilities.formatDate(new Date(smsCampaignMessageStatus.dateSent), Session.getScriptTimeZone(), 'yyyy-MM-dd HH:mm:ss'));
                  resultSheetHandler
                    .getRange(smsCampaignProgressRowIndex, 8)
                      .setValue(smsCampaignMessageStatus.status);
                  resultSheetHandler
                    .autoResizeColumns(1,resultSheetHandler.getLastColumn());
                  break;
                case 'Sent':
                  resultSheetHandler
                    .getRange(smsCampaignProgressRowIndex, 6)
                      .setValue(Utilities.formatDate(new Date(smsCampaignMessageStatus.dateSent), Session.getScriptTimeZone(), 'yyyy-MM-dd HH:mm:ss'));
                  resultSheetHandler
                    .getRange(smsCampaignProgressRowIndex, 8)
                      .setValue(smsCampaignMessageStatus.status);
                  resultSheetHandler
                    .autoResizeColumns(1,resultSheetHandler.getLastColumn());
                  break;
                case 'Accepted':
                case 'Queued':
                case 'Scheduled':
                case 'Sending':
                default:
                  break;
              }
            }
          }

          smsCampaignProgressRowIndex++;
          if (smsCampaignProgressRowIndex > resultSheetHandler.getLastRow())
            smsCampaignProgressRowIndex = 2;
        }
      }
      else {
        console.info(sheetName + ' > runSMSCampaignStatusThread > campaign did not start yet');
        break;
      }
    }
  }
  catch (exception) {
    console.info(sheetName + ' > runSMSCampaignStatusThread > sms campaign status thread error');
    console.error(exception);
  }

  console.info(sheetName + ' > runSMSCampaignStatusThread > end');
}

// -------------------------------------------------------------------------------------------------
// FUNCTIONS - GENERAL PURPOSE

/** 
 * Function that returns triggers that are used by script
 */ 
function getSheetTriggers() {
  var sheetTriggers = ScriptApp.getProjectTriggers();

  var installedTriggers = {
    open : null,
    edit : null,
    runSMSCampaign : null,
    runSMSCampaignStatus : null
  };
  try {
    for (var sheetTriggerIterator = 0; sheetTriggerIterator < sheetTriggers.length; sheetTriggerIterator++) {
      switch(sheetTriggers[sheetTriggerIterator].getEventType()) {
        case ScriptApp.EventType.ON_OPEN:
          if (sheetTriggers[sheetTriggerIterator].getHandlerFunction() == 'triggerOnOpen')
            installedTriggers.open = sheetTriggers[sheetTriggerIterator];
          break;
        case ScriptApp.EventType.ON_EDIT:
            if (sheetTriggers[sheetTriggerIterator].getHandlerFunction() == 'triggerOnEdit')
              installedTriggers.edit = sheetTriggers[sheetTriggerIterator];
          break;
        case ScriptApp.EventType.CLOCK:
          if (sheetTriggers[sheetTriggerIterator].getHandlerFunction() == 'runSMSCampaignThread') {
            installedTriggers.runSMSCampaign = sheetTriggers[sheetTriggerIterator];
          }
          if (sheetTriggers[sheetTriggerIterator].getHandlerFunction() == 'runSMSCampaignStatusThread') {
            installedTriggers.runSMSCampaignStatus = sheetTriggers[sheetTriggerIterator];
          }
          break;
        default:
          console.error(sheetName + '> getSheetTriggers > not recognized event type ' + sheetTriggers[sheetTriggerIterator].getEventType().toString());
          break;
      }
    }
  }
  catch (exception) {
    console.error(sheetName + '> getSheetTriggers > error: ' + exception.toString());
  }

  return installedTriggers;
}

/**
 * Function that returns campaign configuration parameters from UI
 * 
 * @returns object 
 * {
 *    accountSid        - Twilio accound sid 
 *    authToken         - Twilio authentication token
 *    smsFromNumber     - Phone number selected by the user that will be used as presentation number for the sms campaign
 *    smsToNumberColumn - Column that will be used by script as a customer phone number
 *    smsMessage        - SMS message that will be send for customer
 *    dataSheetName     - Name of the sheet from which the data will be pulled from
 * }  
 */
function getSMSCampaignConfigurationParameters() {
    var twilioSheetHandler = sheetHandler.getSheetByName(sheetName);

  // Campaign runtime parameters
    var accountSid = twilioSheetHandler.getRange(ui_baseRowOffset + 1, 2).getValue();
    var authToken = twilioSheetHandler.getRange(ui_baseRowOffset + 2, 2).getValue();
    var smsFromNumber = twilioSheetHandler.getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 3, 2).getValue();
    var smsToNumberColumn = twilioSheetHandler.getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 5, 2).getValue();
    var smsMessage = twilioSheetHandler.getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 3, ui_infoBarWidth - ui_dataColumnWidth + 1).getValue().toString();

  // Data sheet parameters
    var dataSheetName = twilioSheetHandler.getRange(ui_baseRowOffset + ui_smsConfigurationFieldOffset + 4, 2).getDisplayValue();

    return {
      accountSid : accountSid,
      authToken : authToken,
      smsFromNumber : smsFromNumber,
      smsToNumberColumn : smsToNumberColumn,
      smsMessage : smsMessage,
      dataSheetName : dataSheetName
    }
}

/**
 * General function used to verify that the value is null, undefined or empty 
 * 
 * @param {string} value - text that will be checked against undefined, null or empty value
 * @returns boolean (true/false) - value indicates if the input string was empty or filled with data
 */
function isNullOrEmpty(value) {
  if ((value !== undefined) || (value != null)) {
    if (value.length == 0)
      return true;
    else
      return false;
  }
  else
    return true;
}

/**
 * Function returns input text with first letter uppercase
 * 
 * @param {string} text - text for which the uppers case operation will be performed 
 * @returns string - text with first letter uppercase
 */
function setFirstLetterToUpperCase(text) {
  if ((text === undefined) || (text == null) || (text.length == 0))
    return '';
  return text.charAt(0).toUpperCase() + text.slice(1);
}