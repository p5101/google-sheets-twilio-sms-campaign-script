# Google Sheets - Twilio SMS Campaign Script
Twilio SMS Campaign Script allows users to make SMS campaigns directly from the Google Sheet without the need to build infrastructure to host such services. 
Solution uses the power of Google Apps Script service that provides capabilities of automation that can be added to the sheet. All that user needs to do is to
provide customer valid phone numbers (including country code) and start the campaign. The script will manage eveything in background.

# Documentation
All documents that describe the requirements, deployment package, installation procedure and troubleshooting tips can be found in the **Documentation** folder.